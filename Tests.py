import Test 
from solve import get_indices

Test.assert_equals(get_indices(['a', 'a', 'b', 'a', 'b', 'a'], 'a'), [0, 1, 3, 5])
Test.assert_equals(get_indices([1, 5, 5, 2, 7], 7), [4])
Test.assert_equals(get_indices([1, 5, 5, 2, 7], 5),[1, 2])
Test.assert_equals(get_indices([1, 5, 5, 2, 7], 8), [])
Test.assert_equals(get_indices([8, 8, 8, 8, 8], 8), [0, 1, 2, 3, 4])
Test.assert_equals(get_indices([8, 8, 7, 8, 8], 8), [0, 1, 3, 4])
Test.assert_equals(get_indices([True, False, True, False], True), [0, 2])
Test.assert_equals(get_indices([True, False, True, False], False), [1, 3])